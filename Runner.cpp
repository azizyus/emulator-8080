//
// Created by azizyus on 5/16/19.
//

#include "Runner.h"



void Runner::run(State8080 *mcu) {

    while (true)
    {

        while (mcu->currentCycles < mcu->currentFreq)
        {
            mcu->processNextOpcode();

            if( mcu->pc==0) exit(0);

            if( mcu->pc==0x0005)
            {
                mcu->bdos();
            }

        }

        mcu->doIneedSleep();

    }
    printf("\n");
}