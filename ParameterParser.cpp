//
// Created by azizyus on 5/17/19.
//

#include "ParameterParser.h"
#include <iostream>


void ParameterParser::parse(State8080 *mcu,int argc,char **argv)
{
    int next = 0;
    string nextParameter = "";
    //arcg 1 is program name, 2 is filename
    if(argc > 2) mcu->currentFreq = 1; // *= multiplier and *= hertz value thats why i need 1
    else
    {
        mcu->currentFreq = MHZ2;
        return;
    }
    // 1 *= -f 2
    // = 2
    // 2 *= -t mhz
    // = 2000000
    for (int i = 1; i < argc; ++i)
    {
        next = i+1;
        string parameter = (argv)[i];
//        cout << parameter << endl;
        nextParameter = "";
        if(i+1 < argc) nextParameter = (argv)[next];
        if(parameter == "-f")
        {
            mcu->currentFreq *= stoi(nextParameter);
        }
        else if(parameter == "-t")
        {
            if(nextParameter == "khz")
            {
                mcu->currentFreq *= _KHZ_MULTIPLIER;
            }
            else if(nextParameter == "mhz")
            {
                mcu->currentFreq *= _MHZ_MULTIPLIER;
            }
            else if(nextParameter == "ghz")
            {
                mcu->currentFreq *= _GHZ_MULTIPLIER;
            }
        }

    }

}