//
// Created by azizyus on 5/16/19.
//

#ifndef INC_8080EMULATOR_RUNNER_H
#define INC_8080EMULATOR_RUNNER_H

#include "State8080.h"

class Runner {

public:
    void run(State8080 *mcu);
};


#endif //INC_8080EMULATOR_RUNNER_H
