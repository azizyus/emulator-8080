//
// Created by azizyus on 5/17/19.
//

#ifndef INC_8080EMULATOR_PARAMETERPARSER_H
#define INC_8080EMULATOR_PARAMETERPARSER_H

#include "State8080.h"

class ParameterParser {
public:
    void parse(State8080 *mcu,int argc,char **argv);

};


#endif //INC_8080EMULATOR_PARAMETERPARSER_H
