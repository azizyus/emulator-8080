

class InvadersWrapper
{


public:

    uint16_t width = 224;
    uint16_t height = 256;
    uint16_t halfWidth = 112;
    uint16_t halfHeight = 128;

    struct State8080* myState;
    int fps = 30;

    InvadersWrapper(uint16_t width=224,uint16_t height=256,int fps = 30)
    {

        this->width = width;
        this->height = height;
        this->fps = fps;

    }

    void setMcu(State8080 *mcu)
    {
         this->myState = mcu;
    }


    void leftUp()
    {
        myState->port1 &= ~0x20;
    }
    void left()
    {
        myState->port1 |= 0x20;
    }

    void rightUp()
    {
        myState->port1 &= ~0x40;
    }
    void right()
    {
        myState->port1 |= 0x40;
    }


    void fire()
    {
        myState->port1 |= 0x10;
    }
    void fireUp()
    {
        myState->port1 &= ~0x10;
    }

    void coinUp()
    {
        myState->port1 &= 0xFE;
    }
    void coin()
    {
        myState->port1 |= 0x01;
    }

    void singlePlayerStart()
    {
        myState->port1 |= 0x04;
    }
    void singlePlayerStartUp()
    {
        myState->port1 &= ~0x04;
    }


    /**
     *
     * lets assume its 30fps game
     * you have 2mhz 8080 cpu
     * every frame makes 2mhz/30 cycle
     * that's why 1000000/this->fps is necessary to represent 30fps game play
     * basically it checks; enough cycle has been processed in given time? if it is, then sleep so you wont consume all of your cpu
     * and represent 30fps gameplay
     *
     * note: 1000000 microsecond  makes 1 second (@see 8080State::doIntNeedSleep)
     *
     */
    void gameLoop()
    {


        while (myState->currentCycles < myState->currentFreq/this->fps)
        {
            this->interrupt(2);
            myState->processNextOpcode();
            this->interrupt(1);
        }

        myState->sleep((1000000/this->fps));




    }

    std::chrono::steady_clock::time_point lastInterrupt = std::chrono::steady_clock::now();
    void interrupt(int v)
    {

            long difference1 = (std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::steady_clock::now() - lastInterrupt).count());
            if (difference1 >= 1)
            {
                myState->interrupt(v);
                lastInterrupt = std::chrono::steady_clock::now();
            }

    }

    uint8_t getFromVideo(int offset)
    {
        return this->myState->memory[0x2400+offset];
    }

    uint8_t getBit(uint8_t byte, uint8_t position) // position in range 0-7
    {
        return ((byte >> position));
    }


    void drawVram()
    {

            glClear(GL_COLOR_BUFFER_BIT);
            glBegin(GL_POINTS);
            int  vramPointer = 0;

            for (int y = 0; y < 256; y++)
            { //row
                for (int x = this->height; x > 0; x-=8)
                { //column


                    uint8_t pixel8FromVram = this->getFromVideo(vramPointer);

                    for (uint8_t i = 0; i < 8; ++i) {


                       uint8_t isOn = pixel8FromVram & 1 << i;
                       if(isOn)
                       {

                           glVertex2i (y,x-i);
                       }



                    }
                    vramPointer++;





                }


            }

            glEnd();





    }










};

