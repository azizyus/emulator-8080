#include <iostream>
#include <string>
#include "../../State8080.h"
#include <unistd.h>


#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#ifdef _WIN32
  #include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif
//
#include "invaders-wrapper.h"
InvadersWrapper *invadersWrapper;

#include "invaders-opengl-wrapper.h"
InvadersOpenGlWrapper *invadersOpenGlWrapper;

#include "GlutForwarder.h"
#include "../../ParameterParser.h"
#include "../../Flasher.h"
State8080 *myState;
using namespace std;

int main(int argc, char** argv)
{



    myState = new State8080();
    ParameterParser *myParameterCatcher = new ParameterParser();
    Flasher *myFlasher = new Flasher();


    myFlasher->flashFile(myState,argc, argv);
    myParameterCatcher->parse(myState,argc,argv);


    invadersWrapper = new InvadersWrapper();
    invadersWrapper->setMcu(myState);
//    invadersWrapper->fps = 60;
    invadersOpenGlWrapper = new InvadersOpenGlWrapper();


    GlutForwarder *myGlutForwarder = new GlutForwarder;
    myGlutForwarder->startGame();

    return 0;

}
