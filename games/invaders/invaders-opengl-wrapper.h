



void pressKey(unsigned char key, int x, int y){

    if(key == 'a')
        invadersWrapper->left();
    if(key == 'd')
        invadersWrapper->right();
    if(key == 'w')
        invadersWrapper->fire();
    if(key == 'c')
        invadersWrapper->coin();
    if(key == 13)
        invadersWrapper->singlePlayerStart();


}
void pressKeyUp(unsigned char key, int x, int y){

    if(key == 'a')
        invadersWrapper->leftUp();
    if(key == 'd')
        invadersWrapper->rightUp();
    if(key == 'w')
        invadersWrapper->fireUp();
    if(key == 'c')
        invadersWrapper->coinUp();
    if(key == 13)
        invadersWrapper->singlePlayerStartUp();


}
void timer(int fps)
{

    glutPostRedisplay();
    glFlush();
    glutTimerFunc(fps, timer, fps); //3rd param will be passed to timer


}

class InvadersOpenGlWrapper
{

public:



    void initOpenGl(int argc=0, char** argv=0,void (*myDrawFunction)()=0,void (*myGameLoopFunction)()=0,int fps=30)
    {


        glutInit(&argc,argv);
        glutInitDisplayMode(GLUT_SINGLE);
        glutInitWindowSize(224,256);
        glutCreateWindow("8080 space invaders");
        glClearColor(0,0,0,0);
        gluOrtho2D(0,224,256,0);
        glutKeyboardFunc(pressKey);
        glutKeyboardUpFunc(pressKeyUp);

        glPointSize(1);
        glColor3f(1.0f, 1.0f, 1.0f);
        //glutSpecialFunc(pressKey);
        glutDisplayFunc(myDrawFunction);
        glutIdleFunc(myGameLoopFunction);
        timer(1000/fps);
//        glutTimerFunc(0,timer,0);
        glutMainLoop(); //looks like without mainLoop i cant handle key presses

    }


};