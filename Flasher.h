
#ifndef INC_8080EMULATOR_FLASHER_H
#define INC_8080EMULATOR_FLASHER_H

#include "State8080.h"
#include "ROM.h"

class Flasher {

public:
    void flash(State8080 *mcu, unsigned char *bytes, int space = 0, int size=0);
    void flashForCPM(State8080 *mcu,unsigned char *bytes, int size);
    void flashFileForCPM(State8080 *mcu,int argc, char **argv);
    void flashFile(State8080 *mcu,int argc, char **argv);
    ROM* getGameFile(int argc,char **argv);
};


#endif //INC_8080EMULATOR_FLASHER_H
